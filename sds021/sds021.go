package sds021

import (
	"io"
	"log"
)

// SDS021 is representation of UART connected dust sensor
type SDS021 struct {
	port      io.ReadWriteCloser
	callbacks Callbacks
}

type Callbacks struct {
	versionCallback           func(day, month, year int)
	dataCallback              func(pm25, pm10 float64)
	dataReportingModeCallback func(mode ReportingMode)
	workingPeriodCallback     func(period int)
	sleepWorkStateCallback    func(mode WorkingMode)
}

const (
	commandIDIndex   = 1
	commandTypeIndex = 2

	commandReply = 0xC5
	dataReport   = 0xC0

	dataReportingMode = 2
	sleepAndWork      = 6
	firmwareVersion   = 7
	workingPeriod     = 8
	queryData         = 4

	notUsed byte = 0

	query = 0
	set   = 1
)

// New creates instance of SDS021, it needs opened serial port
func New(port io.ReadWriteCloser, dc func(pm25, pm10 float64)) *SDS021 {
	sds021 := new(SDS021)
	if port != nil {
		sds021.port = port
	} else {
		// new default port
		log.Fatal("No Port, No Fun")
	}
	sds021.callbacks = Callbacks{dataCallback: dc}
	return sds021
}

// sum all data fields (byte 2-16)
func (sds *SDS021) computeChecksum(data []byte) byte {
	checksum := byte(0)
	for idx, value := range data {
		if idx > 1 && idx < 17 {
			checksum = checksum + value
		}
	}
	return checksum
}

// writes data to sensor's port
func (sds *SDS021) sendToPort(data []byte) {
	_, err := sds.port.Write(data)
	if err != nil {
		log.Fatalf("port.Write: %v", err)
	}
}

// effectively only 3 fields are changed in SDS021 commands, rest is same
// this function computes and sets checksum as well
func (sds *SDS021) buildMessage(third, fourth, fifth byte) []byte {
	commandTemplate := []byte{0xAA, 0xB4, third, fourth, fifth,
		0, 0, 0, 0, 0,
		0, 0, 0, 0, 0,
		0xFF, 0xFF, 0xCC, 0xAB}
	commandTemplate[17] = sds.computeChecksum(commandTemplate)
	return commandTemplate
}

// ReadData expect 10 bytes of data, process it, and eventually call users callback with them
func (sds *SDS021) ReadData() {
	for {
		data := make([]byte, 10)
		len, err := sds.port.Read(data)
		if err != nil {
			log.Println(err)
			break
		}
		if len != 10 {
			log.Println("Data is not 10 bytes long, trashing and waiting for another")
			break
		}

		sds.processData(data)
	}
}

// SetWorkingPeriod period = 0 is continuos reporting, period > 0 is 30s work, n*60-30s sleep
func (sds *SDS021) SetWorkingPeriod(period int) {
	sds.messageWorkingPeriod(set, byte(period))
}

//GetWorkingPeriod sends command after which sensor reply with Working peroid state of sensor
func (sds *SDS021) GetWorkingPeriod(wpc func(period int)) {
	sds.callbacks.workingPeriodCallback = wpc
	sds.messageWorkingPeriod(query, notUsed)
}

func (sds *SDS021) messageWorkingPeriod(action, period byte) {
	command := sds.buildMessage(workingPeriod, action, period)
	sds.sendToPort(command)
}

type ReportingMode byte

//Active reports values every second
//Query reports value only on request
const (
	Active ReportingMode = 0
	Query  ReportingMode = 1
)

//SetDataReportingMode sets one of Active or Query mode for the sensor
func (sds *SDS021) SetDataReportingMode(mode ReportingMode) {
	sds.messageDataReportingMode(set, mode)
}

//GetDataReportingMode sends command after which sensor reply with Reporting Mode state of sensor
func (sds *SDS021) GetDataReportingMode(rmc func(mode ReportingMode)) {
	sds.callbacks.dataReportingModeCallback = rmc
	sds.messageDataReportingMode(query, ReportingMode(notUsed))
}

func (sds *SDS021) messageDataReportingMode(action, mode ReportingMode) {
	command := sds.buildMessage(dataReportingMode, byte(action), byte(mode))
	sds.sendToPort(command)
}

//QueryData requests data in query mode of sensor
func (sds *SDS021) QueryData() {
	command := sds.buildMessage(queryData, notUsed, notUsed)
	sds.sendToPort(command)
}

// WorkingMode introduces some type checking for functions using it
type WorkingMode byte

// Sleep turns laser and fan off
// Work turns laser and fan on, values should be stabilised after 30s
const (
	Sleep WorkingMode = 0
	Work  WorkingMode = 1
)

func (sds *SDS021) SetSleepAndWork(state WorkingMode) {
	sds.messageSleepAndWork(set, state)
}

func (sds *SDS021) GetSleepAndWork(swc func(mode WorkingMode)) {
	sds.callbacks.sleepWorkStateCallback = swc
	sds.messageSleepAndWork(query, WorkingMode(notUsed))
}

func (sds *SDS021) messageSleepAndWork(mode byte, state WorkingMode) {
	command := sds.buildMessage(sleepAndWork, mode, byte(state))
	sds.sendToPort(command)
}

// GetFirmwareVersion returns year, month and day of sensor's version
func (sds *SDS021) GetFirmwareVersion(vc func(day, month, year int)) {
	sds.callbacks.versionCallback = vc
	command := sds.buildMessage(firmwareVersion, notUsed, notUsed)
	sds.sendToPort(command)
}

func (sds *SDS021) processData(data []byte) {

	switch data[commandIDIndex] {
	case commandReply:
		switch data[commandTypeIndex] {
		case firmwareVersion:
			sds.processFirmwareVersionReply(data)
		case dataReportingMode:
			sds.processDataReportingModeReply(data)
		case sleepAndWork:
			sds.processSleepAndWorkReply(data)
		case workingPeriod:
			sds.processWorkingPeriodReply(data)
		default:
			log.Println("Unkwnown command reply, ignoring this data")
			log.Printf("%v\n", data)
		}
	case dataReport:
		sds.processPMReport(data)
	default:
		log.Println("Unkwnown Command ID, ignoring this data")
		log.Printf("%v\n", data)
	}
}

func (sds *SDS021) processPMReport(data []byte) {
	const (
		pm25low  = 2
		pm25high = 3
		pm10low  = 4
		pm10high = 5
	)
	pm25value := float64(int(data[pm25high])*256+int(data[pm25low])) / 10
	pm10value := float64(int(data[pm10high])*256+int(data[pm10low])) / 10
	log.Printf("Measured data: PM2.5 %.1fµg/m³, PM10 %.1fµg/m³\n", pm25value, pm10value)
	if sds.callbacks.dataCallback != nil {
		sds.callbacks.dataCallback(pm25value, pm10value)
	}
	return
}

func (sds *SDS021) processFirmwareVersionReply(data []byte) {
	log.Printf("Firmware date: %d.%d.20%d\n", data[5], data[4], data[3])
	if sds.callbacks.versionCallback != nil {
		sds.callbacks.versionCallback(int(data[5]), int(data[4]), int(data[3]))
	}
	return
}

func (sds *SDS021) processSleepAndWorkReply(data []byte) {
	state := WorkingMode(data[4])
	log.Printf("Query mode/Set mode: %d\n", data[3])
	log.Printf("Sleep/Work state: %d\n", state)
	if sds.callbacks.sleepWorkStateCallback != nil {
		sds.callbacks.sleepWorkStateCallback(WorkingMode(state))
	}
	return
}

func (sds *SDS021) processDataReportingModeReply(data []byte) {
	action := data[3]
	mode := ReportingMode(data[4])
	log.Printf("Query mode/Set mode: %d\n", action)
	log.Printf("Active/Query mode: %d\n", mode)
	if sds.callbacks.dataReportingModeCallback != nil {
		sds.callbacks.dataReportingModeCallback(mode)
	}
	return
}

func (sds *SDS021) processWorkingPeriodReply(data []byte) {
	action := data[3]
	period := data[4]
	log.Printf("Query mode/Set mode: %d\n", action)
	log.Printf("Continuous/Every x minutes: %d\n", period)
	if sds.callbacks.workingPeriodCallback != nil {
		sds.callbacks.workingPeriodCallback(int(period))
	}
	return
}
