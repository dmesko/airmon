package main

import (
	"fmt"
	"log"
	"time"

	"periph.io/x/periph/conn/i2c"
	"periph.io/x/periph/conn/i2c/i2creg"
	"periph.io/x/periph/conn/physic"
	"periph.io/x/periph/devices/bmxx80"
	"periph.io/x/periph/experimental/devices/ccs811"
	"periph.io/x/periph/host"
)

func main() {
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	b, err := i2creg.Open("")
	if err != nil {
		log.Fatalf("failed to open I²C: %v", err)
	}
	defer b.Close()

	ccs811dev, err := ccs811.New(b, &ccs811.Opts{Addr: 0x5A, MeasurementMode: ccs811.MeasurementModeConstant1000})
	bmp280dev := getBMP280(b)
	for {
		bmp280Values := &physic.Env{}
		err := bmp280dev.Sense(bmp280Values)
		if err != nil {
			log.Println("BMP280 Error, waiting for next value", err)
		} else {
			fmt.Println("Pressure:", bmp280Values.Pressure, "Temperature:", bmp280Values.Temperature)
		}

		ccs811Values := &ccs811.SensorValues{}
		err = ccs811dev.SensePartial(ccs811.ReadCO2VOC, ccs811Values)
		if err != nil {
			log.Println("CCS811 Error, waiting for next value", err)
		} else {
			fmt.Println("eCO2:", ccs811Values.ECO2, "VOC:", ccs811Values.VOC)
		}
		time.Sleep(1200 * time.Millisecond)
	}
}

func VOCAndeCO2(dev *Dev, data chan map[string]float64) {
	for {
		ccs811Values := &ccs811.SensorValues{}
		err := dev.SensePartial(ccs811.ReadCO2VOC, ccs811Values)
		if err != nil {
			log.Println("CCS811 Error, waiting for next value", err)
		} else {
			fmt.Println("eCO2:", ccs811Values.ECO2, "VOC:", ccs811Values.VOC)
		}
		time.Sleep(1200 * time.Millisecond)
	}
}

func PressureAndTemperature(dev *bmxx80.Dev, data chan *physic.Env) {
	for {
		bmp280Values := &physic.Env{}
		err := dev.Sense(bmp280Values)
		if err != nil {
			log.Println("BMP280 Error, waiting for next value", err)
		} else {
			data <- bmp280Values
			fmt.Println("Pressure:", bmp280Values.Pressure, "Temperature:", bmp280Values.Temperature)
		}
		time.Sleep(1200 * time.Millisecond)
	}
}

func getBMP280(b i2c.BusCloser) *bmxx80.Dev {
	opts := &bmxx80.Opts{
		Temperature: bmxx80.O1x,
		Pressure:    bmxx80.O8x,
		Filter:      bmxx80.F4,
	}

	bmp280, err2 := bmxx80.NewI2C(b, 0x76, opts)
	if err2 != nil {
		fmt.Println(err2)
		return nil
	}

	return bmp280
}
