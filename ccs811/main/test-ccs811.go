package main

import (
	"fmt"
	"log"
	"time"

	"periph.io/x/periph/conn/i2c"
	"periph.io/x/periph/conn/i2c/i2creg"
	"periph.io/x/periph/host"
)

func main() {
	// Make sure periph is initialized.
	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	// Use i2creg I²C bus registry to find the first available I²C bus.
	b, err := i2creg.Open("")
	if err != nil {
		log.Fatalf("failed to open I²C: %v", err)
	}
	defer b.Close()

	d := i2c.Dev{b, 0x5A}

	// from boot mode to measurement mode
	err = d.Tx([]byte{0xf4}, nil)
	if err != nil {
		fmt.Println("Write error: ", err)
	}
	time.Sleep(30 * time.Millisecond)

	_, err = d.Write([]byte{0x01, 0x10})
	if err != nil {
		fmt.Println("Write error: ", err)
	}
	time.Sleep(30 * time.Millisecond)

	for {
		readBuffer := make([]byte, 5)
		write := []byte{0x02}
		if err := d.Tx(write, readBuffer); err != nil {
			log.Fatal(err)
		}
		eCO := (uint32(readBuffer[0]) << 8) | uint32(readBuffer[1])
		if eCO > 4000 {
			eCO = eCO & 0X7FFF
		}
		voc := (uint32(readBuffer[2]) << 8) | uint32(readBuffer[3])
		fmt.Println("eCO2", eCO, "VOC", voc)
		// fmt.Printf("%v\n", readBuffer)
		// printAsHex(readBuffer)
		// printAsBin(readBuffer)
		time.Sleep(1200 * time.Millisecond)
	}

}

// pi@silak:~/go/src/github.com/google/periph/cmd/i2c-io $ ./i2c-io -a 0x5a -l 4 -r 4
func printAsHex(buf []byte) {
	for _, b := range buf {
		fmt.Printf("%x ", b)
	}
	fmt.Println()
}

func printAsBin(buf []byte) {
	for _, b := range buf {
		fmt.Printf("%b ", b)
	}
	fmt.Println()
}
